package id.co.asyst.amala.mail.management.verification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@EnableJms
@ComponentScan({"id.co.asyst.commons", "id.co.asyst.amala"})
@EntityScan({"id.co.asyst.amala"})
public class application {

    public static void main(String[] args){
        SpringApplication.run(application.class, args);
    }
}
