package id.co.asyst.amala.mail.management.verification.config;

import id.co.asyst.amala.mail.management.verification.repository.MailVerificationRepository;
import id.co.asyst.amala.member.model.Member;
import id.co.asyst.commons.core.component.Translator;
import id.co.asyst.commons.core.exception.ApplicationException;
import id.co.asyst.commons.core.payload.BaseRequest;
import id.co.asyst.commons.core.payload.BaseResponse;
import id.co.asyst.commons.core.payload.Identity;
import id.co.asyst.commons.core.payload.Status;
import id.co.asyst.commons.core.service.BaseService;
import id.co.asyst.commons.core.utils.DateUtils;
import jnr.a64asm.Mem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Map;

@Service
public class MailVerificationServiceImpl extends BaseService<Member, String> implements MailVerificationService {

    private MailVerificationRepository mailVerificationRepository;

    @Autowired
    public MailVerificationServiceImpl(MailVerificationRepository mailVerificationRepository) {
        this.mailVerificationRepository = mailVerificationRepository;
        this.setRepository(mailVerificationRepository);
    }

    private Logger logger = (Logger) LoggerFactory.getLogger(MailVerificationServiceImpl.class);

    @Override
    public Object update(Map<String, Object> mail) {
        logger.info("masuk service");

        String email = mail.get("email").toString();
        logger.info("::: EMAIL :::" + email);
        String type = mail.get("type").toString();
        logger.info("::: TYPE :::" + type);

        try {
            Member member = mailVerificationRepository.findByEmail(email);

            if (member == null)
                throw new ApplicationException(Status.DATA_NOT_FOUND("not found"));
            logger.info("email not found");

            if (type.contains("delivered") && type.contains("deffered") && type.contains("blocked")) {
                member.setEmailverified(true);
                member.setUpdatedDate(DateUtils.now());
            }
            if (type.contains("bounce") && type.contains("dropped")) {
                member.setEmailverified(false);
                member.setUpdatedDate(DateUtils.now());
            }

        } catch (Exception e) {
            logger.info("error : " + e.getMessage());
        }

        return email;
    }
}
