package id.co.asyst.amala.mail.management.verification.model;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class WebHookEvent {

    public final String email;
    public final String event;
    public final String sg_event_id;
    public final String sg_message_id;
    @JsonProperty("smtp-id")
    public final String smtpId;
    public final int timestamp;

    public WebHookEvent(@JsonProperty("email") String email,
                        @JsonProperty("event") String event,
                        @JsonProperty("sg_event_id") String sg_event_id,
                        @JsonProperty("sg_message_id") String sg_message_id,
                        @JsonProperty("smtp-id") String smtpId,
                        @JsonProperty("timestamp") int timestamp) {
        this.email = email;
        this.event = event;
        this.sg_event_id = sg_event_id;
        this.sg_message_id = sg_message_id;
        this.smtpId = smtpId;
        this.timestamp = timestamp;
    }

    public String getEmail() {
        return email;
    }

    public String getEvent() {
        return event;
    }

    public String getSg_event_id() {
        return sg_event_id;
    }

    public String getSg_message_id() {
        return sg_message_id;
    }

    public String getSmtpId() {
        return smtpId;
    }

    public int getTimestamp() {
        return timestamp;
    }
}