package id.co.asyst.amala.mail.management.verification.repository;
import id.co.asyst.amala.member.model.Member;
import id.co.asyst.commons.core.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MailVerificationRepository extends BaseRepository<Member, String> {

    Member findByEmail(String email);
    void deleteByEmail(String email);


}