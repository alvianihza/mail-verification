package id.co.asyst.amala.mail.management.verification.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.asyst.amala.mail.management.verification.config.MailVerificationService;
import id.co.asyst.amala.mail.management.verification.model.WebHookEvent;
import id.co.asyst.amala.mail.management.verification.repository.MailVerificationRepository;
import id.co.asyst.commons.core.payload.BaseRequest;
import id.co.asyst.commons.core.payload.BaseResponse;
import id.co.asyst.commons.core.payload.Identity;
import id.co.asyst.commons.core.payload.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class JmsConsumer {

    private static Logger logger = LoggerFactory.getLogger(JmsConsumer.class);

    @Autowired
    MailVerificationService mailVerificationService;

    @Autowired
    MailVerificationRepository mailVerificationRepository;

    @JmsListener(destination = "mail.verification", containerFactory = "jsaFactory")
    public void receive(String message) throws ParseException, IOException {
        ObjectMapper oMapper = new ObjectMapper();


        List<WebHookEvent> events = oMapper.readValue(message, new TypeReference<List<WebHookEvent>>() {
        });
        Map<String, Object> mail = new HashMap<>();
        String email;
        String type;
        if (events.size() > 0) {
            for (WebHookEvent event : events) {
                if (event.getSg_message_id() != null) {
//                    logger.info("email : " + event.getEmail() + " | type : " + event.getEvent());
                    mail.put("email", event.getEmail());
                    mail.put("type", event.getEvent());
//                    email = event.getEmail();
//                    type = event.getEvent();
//                    logger.info("email :" +email);
//                    logger.info("type :" +type);
                    mailVerificationService.update(mail);
//
//                    logger.info("email : " + event.getEmail() + " | type : " + event.getEvent());
//                    String result[] = event.getSg_message_id().split("\\.");
                }
            }
        }

    }


}
