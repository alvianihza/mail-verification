package id.co.asyst.amala.mail.management.verification.config;

import id.co.asyst.amala.member.model.Member;
import id.co.asyst.commons.core.payload.Identity;
import id.co.asyst.commons.core.service.Service;

import java.util.List;
import java.util.Map;

public interface MailVerificationService extends Service<Member, String> {
    Object update(Map<String, Object> mail);
}
